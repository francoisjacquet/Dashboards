<?php
/**
 * Dashboard
 *
 * @package Dashboards module
 */

if ( ! empty( $_REQUEST['id'] ) )
{
	$title = DBGetOne( "SELECT TITLE
		FROM resources_dashboards
		WHERE ID='" . (int) $_REQUEST['id'] . "'" );

	// Hide title, will still serve for browser tab title and accessibility.
	echo '<h2 class="a11y-hidden">' . $title . '</h2>';

	$iframes_RET = DBGet( "SELECT ID,URL,WIDTH,HEIGHT,OPTIONS
		FROM resources_dashboard_elements
		WHERE DASHBOARD_ID='" . (int) $_REQUEST['id'] . "'
		AND TYPE='iframe'
		ORDER BY SORT_ORDER IS NULL,SORT_ORDER,ID" );

	foreach ( (array) $iframes_RET as $iframe )
	{
		// Add `&dahsboard_element=ID` to URL
		$url = $iframe['URL'] . ( mb_strpos( $iframe['URL'], '?' ) !== false ? '&' : '?' ) .
			'dashboard_element=' . $iframe['ID'];

		$iframe_id = 'dashboard-element-iframe' . (int) $iframe['ID'];

		// iframe
		echo '<iframe border="0" width="' . AttrEscape( $iframe['WIDTH'] . '%' ) . '" style="' .
			AttrEscape( 'float: left; padding-top: 12px; height: ' . $iframe['HEIGHT'] . 'px;' ) . '" src="' .
			URLEscape( $url ) . '" id="' . AttrEscape( $iframe_id ) . '"></iframe>';

		$refresh_seconds = 0;

		if ( $iframe['OPTIONS'] )
		{
			$options = json_decode( $iframe['OPTIONS'], true );

			if ( ! empty( $options['REFRESH_AFTER_MINUTES'] ) )
			{
				$refresh_seconds = $options['REFRESH_AFTER_MINUTES'] * 60 * 1000;
			}
		}

		if ( $refresh_seconds ) :

			// JSON variables used by Dashboard.js
			$value = [ 'iframe_id' => $iframe_id, 'refresh_seconds' => $refresh_seconds ];
			?>
			<input type="hidden" disabled class="dashboard-iframe-refresh" value="<?php echo AttrEscape( json_encode( $value ) ); ?>" />
			<?php
		endif;
	}

	// Refresh Dashboard iframe after X seconds
	?>
	<script src="modules/Dashboards/js/Dashboard.js?v=1.3"></script>
	<?php
}
