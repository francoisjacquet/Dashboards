<?php
/**
 * Functions
 *
 * @package Dashboards module
 */

add_action( 'Warehouse.php|footer', 'DashboardElementInjectCSS' );

function DashboardElementInjectCSS()
{
	if ( empty( $_REQUEST['dashboard_element'] ) )
	{
		return false;
	}

	// Get Dashboard element CSS.
	$options = DBGetOne( "SELECT OPTIONS
		FROM resources_dashboard_elements
		WHERE ID='" . (int) $_REQUEST['dashboard_element'] . "'" );

	if ( ! $options )
	{
		return false;
	}

	$options = json_decode( $options, true );

	if ( empty( $options['CSS'] ) )
	{
		return false;
	}

	?>
	<style>
		<?php echo $options['CSS']; ?>
	</style>
	<?php

	return true;
}
