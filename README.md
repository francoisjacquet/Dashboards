Dashboards module
=================

![screenshot](https://gitlab.com/francoisjacquet/Dashboards/raw/master/screenshot.png?inline=false)

https://gitlab.com/francoisjacquet/Dashboards/

Version 1.5 - February, 2025

License GNU/GPLv2 or later

Author François Jacquet

Sponsored by AT group, Slovenia

DESCRIPTION
-----------
Design your own dashboard by including various RosarioSIS programs / reports on the same page.

For each program / element of the dashboard, the following options are available:

- URL
- Width in percent
- Height in pixel
- Refresh after (minutes)
- CSS: add your own CSS rules to, for example, hide parts of the screen, list columns, headers, buttons, etc.

Note: To disable responsive list layout, add `&LO_disable_responsive=Y` to the URL.

Note 2: you can also have your own custom page displayed on the dashboard. Simply upload the HTML or PHP file on your server and enter its URL. This can be used to display custom buttons, widgets or information from other sources.

Translated in French, Spanish and Slovenian.

You will find the settings for the example dashboard in the [EXAMPLE.md](https://gitlab.com/francoisjacquet/Dashboards/-/blob/master/EXAMPLE.md) file.

CONTENT
-------
Resources
- Dashboards

INSTALL
-------
Copy the `Dashboards/` folder (if named `Dashboards-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School > Configuration > Modules_ and click "Activate".

Requires RosarioSIS 11.3+
