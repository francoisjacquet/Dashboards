��    	      d      �       �   	   �      �      �   
             '  W   ?  	   �  �  �     �     �     �     �     �     �  p        �        	                                      Dashboard Dashboard Element Dashboard Elements Dashboards Height (px) Refresh after (minutes) To disable responsive list layout, add <code>&LO_disable_responsive=Y</code> to the URL Width (%) Project-Id-Version: Dashboards module
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-11-17 16:50+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 2.4.2
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Tableau de bord Élément du tableau de bord Éléments du tableau de bord Tableaux de bord Hauteur (px) Rafraîchir après (minutes) Pour désactiver l’affichage "responsive" des listes, ajoutez <code>&LO_disable_responsive=Y</code> à l’URL Largeur (%) 