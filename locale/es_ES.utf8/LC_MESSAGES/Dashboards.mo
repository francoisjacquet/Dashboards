��    	      d      �       �   	   �      �      �   
             '  W   ?  	   �  �  �     �     �     �     �     �       p   %  	   �        	                                      Dashboard Dashboard Element Dashboard Elements Dashboards Height (px) Refresh after (minutes) To disable responsive list layout, add <code>&LO_disable_responsive=Y</code> to the URL Width (%) Project-Id-Version: Dashboards module
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-11-17 16:48+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 2.4.2
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Tablero de mandos Elemento del tablero de mandos Elementos del tablero de mandos Tableros de mandos Altura (px) Refrescar después de (minutos) Para desactivar la disposición "responsive" de las listas, agregue <code>&LO_disable_responsive=Y</code> al URL Ancho (%) 