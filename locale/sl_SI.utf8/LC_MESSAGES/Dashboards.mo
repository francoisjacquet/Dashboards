��    	      d      �       �   	   �      �      �   
             '  W   ?  	   �  �  �     �     �     �     �     �     �  i        y        	                                      Dashboard Dashboard Element Dashboard Elements Dashboards Height (px) Refresh after (minutes) To disable responsive list layout, add <code>&LO_disable_responsive=Y</code> to the URL Width (%) Project-Id-Version: Dashboards module
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-12-12 15:30+0100
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.4.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Nadzorna plošča Element nadzorne plošče Elementi nadzorne plošče Nadzorne plošče Višina (px) Osveži po (minutah) Če želite onemogočiti odzivno postavitev seznama, URL-ju dodajte <code>&LO_disable_responsive=Y</code> Širina (%) 