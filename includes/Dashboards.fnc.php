<?php
/**
 * Dashboards functions
 *
 * @package Dashboards module
 */

/**
 * Make Text Input
 *
 * DBGet() callback
 *
 * @param string $value
 * @param string $column
 *
 * @return string Text Input
 */
function DashboardsMakeTextInput( $value, $column = 'TITLE' )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	if ( $column === 'URL' )
	{
		$extra = 'size="32" maxlength="1000"';
	}

	if ( $column === 'TITLE' )
	{
		$extra = 'maxlength="30"';
	}

	if ( $column === 'SORT_ORDER' )
	{
		$extra = 'type="number" min="-9999" max="9999"';
	}

	if ( $id !== 'new'
		&& $column !== 'SORT_ORDER' )
	{
		$extra .= ' required';
	}

	return TextInput( $value, 'values[' . $id . '][' . $column . ']', '', $extra );
}

/**
 * Make URL
 *
 * DBGet() callback
 *
 * @uses DashboardsMakeTextInput()
 *
 * @param string $value
 * @param string $name
 *
 * @return string URL input HTML
 */
function DashboardsMakeURL( $value, $name = 'URL' )
{
	global $THIS_RET;

	if ( ! empty( $_REQUEST['LO_save'] ) )
	{
		// Export list.
		return $value;
	}

	if ( $value )
	{
		// Truncate links > 90 chars.
		$truncated_link = $value;

		if ( mb_strlen( $truncated_link ) > 90 )
		{
			$separator = '/.../';
			$separator_length = mb_strlen( $separator );
			$max_length = 90 - $separator_length;
			$start = (int) ( $max_length / 2 );
			$trunc = mb_strlen( $truncated_link ) - $max_length;
			$truncated_link = substr_replace( $truncated_link, $separator, $start, $trunc );
		}

		// Add `&dahsboard_element=ID` to URL
		$link = $value . ( mb_strpos( $value, '?' ) !== false ? '&' : '?' ) .
			'dashboard_element=' . $THIS_RET['ID'];

		return '<div style="display:table-cell;"><a href="' . URLEscape( $link ) . '" target="_blank">' .
			_( 'Link' ) . '</a>&nbsp;</div>
			<div style="display:table-cell;">' . DashboardsMakeTextInput( [ $value, $truncated_link ], $name ) . '</div>';
	}

	return DashboardsMakeTextInput( $value, $name );
}


/**
 * Make Number Input
 *
 * DBGet() callback
 *
 * @param string $value
 * @param string $column
 *
 * @return string Number Input
 */
function DashboardsMakeNumberInput( $value, $column = 'WIDTH' )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	if ( $column === 'WIDTH' ) // %
	{
		$extra = 'type="number" min="20" max="100"';
	}

	if ( $column === 'HEIGHT' ) // px
	{
		$extra = 'type="number" min="100" max="9999"';
	}

	if ( $id !== 'new' )
	{
		$extra .= ' required';
	}

	return TextInput( $value, 'values[' . $id . '][' . $column . ']', '', $extra );
}

/**
 * Make Options
 * Refresh after (minutes)
 * CSS
 *
 * DBGet() callback
 *
 * @param string $value
 * @param string $column
 *
 * @return string Options
 */
function DashboardsMakeOptions( $value, $column = 'OPTIONS' )
{
	global $THIS_RET;

	$id = ! empty( $THIS_RET['ID'] ) ? $THIS_RET['ID'] : 'new';

	if ( $value )
	{
		$value = json_decode( $value, true );
	}

	$refresh_after_minutes = TextInput(
		( isset( $value['REFRESH_AFTER_MINUTES'] ) ? $value['REFRESH_AFTER_MINUTES'] : '' ),
		'values[' . $id . '][' . $column . '][REFRESH_AFTER_MINUTES]',
		dgettext( 'Dashboards', 'Refresh after (minutes)' ),
		'type="number" min="1" max="99"',
		false
	);

	$css = TextAreaInput(
		( isset( $value['CSS'] ) ? $value['CSS'] : '' ),
		'values[' . $id . '][' . $column . '][CSS]',
		dgettext( 'Dashboards', 'CSS' ),
		'',
		false,
		'text'
	);

	$options = $refresh_after_minutes . '<br />' . $css;

	// Fix responsive rt td too large.
	return '<div id="divDashboardElementOptions' . $id . '" class="rt2colorBox"><div>' .
		$options . '</div></div>';
}

/**
 * Make Configuration link
 *
 * DBGet() callback
 *
 * @uses DashboardsMakeTextInput()
 *
 * @param string $value
 * @param string $column
 *
 * @return string Configuration link
 */
function DashboardsMakeConfiguration( $value, $column = 'CONFIGURATION' )
{
	$link = 'Modules.php?modname=' . $_REQUEST['modname'] . '&id=' . $value;

	return '<a href="' . URLEscape( $link ) . '">' . _( 'Configuration' ) . '</a>';
}


/**
 * Reload left menu so Resource (dis)appears
 *
 * @return bool True
 */
function DashboardsReloadMenu()
{
	?>
	<script src="modules/Dashboards/js/DashboardsReloadMenu.js?v=1.3"></script>
	<?php

	return true;
}

/**
 * Sanitize CSS
 *
 * @uses Wikimedia\CSS\Parser\Parser
 * @uses Wikimedia\CSS\Sanitizer\StylesheetSanitizer
 *
 * @link https://github.com/wikimedia/css-sanitizer
 *
 * @param string $css CSS to sanitize.
 *
 * @return string Sanitized CSS.
 */
function DashboardsSanitizeCSS( $css )
{
	// Fix returning "rn" instead of "\r\n".
	$css_to_parse = str_replace( [ "\r\n", "\n", "\r" ], "/*rn*/", $css );

	/** Parse a stylesheet from a string **/

	$parser = Wikimedia\CSS\Parser\Parser::newFromString( $css_to_parse );
	$stylesheet = $parser->parseStylesheet();

	/** Apply sanitization to the stylesheet **/

	// If you need to customize the defaults, copy the code of this method and
	// modify it.
	$sanitizer = Wikimedia\CSS\Sanitizer\StylesheetSanitizer::newDefault();
	$new_stylesheet = $sanitizer->sanitize( $stylesheet );

	/** Convert the sanitized stylesheet back to text **/

	$sanitized_css = (string) $new_stylesheet;

	if ( ! $sanitizer->getSanitizationErrors() )
	{
		return $css;
	}

	return $sanitized_css;
}
