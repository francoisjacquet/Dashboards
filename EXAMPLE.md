Dashboard example
=================

Here is an example dashboard layout adapted to a 1920x1080px screen.

It requires the _Entry and Exit_ and the _Slovenian Class Diary_ add-on modules for RosarioSIS.

1. Student Records for today since midnight, Gate (checkpoint ID 1), type "Entry"

- URL: `Modules.php?modname=Entry_Exit/Records.php&type=student&start_time=00:00&checkpoint_id=1&record_type=in&LO_disable_responsive=Y`
- Width: 50%
- Height: 400px
- CSS:
```css
#wrap { padding:0; } /*Hide menu space*/

#footer, #menu, /*Hide menu*/
.header2, /*Hide headers*/
.list-nav, /*Hide list search*/
.list th:nth-of-type(1), /*Hide 1st column*/
.list td:nth-of-type(1),
.list th:nth-of-type(4), /*Hide 4th column*/
.list td:nth-of-type(4),
input[type="submit"] /*Hide submit button*/
{ display: none; }
```

2. Evening Leave for today, Return Time: 20:00, Gate (checkpoint ID 1), type Entry, hide students without evening leave:

- URL: `Modules.php?modname=Entry_Exit/EveningLeave.php&checkpoint_id=1&record_type=in&return_time=20:00&LO_disable_responsive=Y&hide_students_without_evening_leave=Y`
- Width: 50%
- Height: 400px
- CSS:
```css
#wrap { padding:0; } /*Hide menu space*/

#footer, #menu, /*Hide menu*/
.header2, /*Hide headers*/
.list-nav, /*Hide list search*/
.list th:nth-of-type(2), /*Hide 1st column*/
.list td:nth-of-type(2)
{ display: none; }
```

3. On Call Log for the last 15 days

- URL: `Modules.php?modname=Slovenian_Class_Diary/OnCallLogDay.php&LO_disable_responsive=Y`
- Width: 70%
- Height: 400px
- CSS:
```css
#wrap { padding:0; } /*Hide menu space*/

#footer, #menu /*Hide menu*/
{ display: none; }
```

4. Package Delivery: student who have a package to pickup list

- URL: `Modules.php?modname=Entry_Exit/PackageDelivery.php&modfunc=&search_modfunc=list&has_package_to_pickup=Y&LO_disable_responsive=Y`
- Width: 30%
- Height: 400px
- CSS:
```css
#wrap { padding:0; } /*Hide menu space*/

#footer, #menu, /*Hide menu*/
.header2, /*Hide headers*/
.postbox, /*Hide form*/
form br, /*Hide line breaks inside form*/
.list-nav, /*Hide list search*/
.list th:nth-of-type(1), /*Hide 1st column*/
.list td:nth-of-type(1),
.list th:nth-of-type(3), /*Hide 3rd column*/
.list td:nth-of-type(3),
input[type="submit"] /*Hide submit button*/
{ display: none; }
```
