/**
 * Dashboard program JS
 *
 * @package Dashboards module
 */

// Refresh Dashboard iframe after X seconds
$('.dashboard-iframe-refresh').each(function() {
	var json = JSON.parse(this.value),
		iframe = document.getElementById(json.iframe_id),
		refresh_seconds = json.refresh_seconds;

	if (! iframe || ! refresh_seconds) {
		return;
	}

	window.setInterval(function() {
		iframe.src = iframe.src;
	}, refresh_seconds);
});
