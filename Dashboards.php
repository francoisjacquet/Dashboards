<?php
/**
 * Dashboards
 *
 * @package Dashboards module
 */

require_once 'modules/Dashboards/includes/Dashboards.fnc.php';

if ( version_compare( PHP_VERSION, '7.4.0', '<' ) )
{
	require_once 'modules/Dashboards/css-sanitizer-2.0.1/vendor/autoload.php';
}
else
{
	require_once 'modules/Dashboards/css-sanitizer-5.4.0/vendor/autoload.php';
}

DrawHeader( ProgramTitle() );

if ( $_REQUEST['modfunc'] === 'update' )
{
	if ( ! empty( $_REQUEST['values'] )
		&& ! empty( $_POST['values'] )
		&& AllowEdit() )
	{
		foreach ( (array) $_REQUEST['values'] as $id => $columns )
		{
			if ( $id !== 'new' )
			{
				DBUpdate(
					'resources_dashboards',
					$columns,
					[ 'ID' => (int) $id ]
				);
			}

			// New: check for Title.
			elseif ( $columns['TITLE'] )
			{
				$resource_id = DBInsert(
					'resources_dashboards',
					$columns,
					'id'
				);

				if ( $resource_id )
				{
					$modname = 'Dashboards/Dashboard.php&id=' . $resource_id;

					// Admin can Use Resource.
					DBQuery( "INSERT INTO profile_exceptions (PROFILE_ID,MODNAME,CAN_USE,CAN_EDIT)
						values('1','" . $modname . "','Y','Y');" );
				}
			}

			// Reload left menu so Resource appears.
			DashboardsReloadMenu();
		}
	}

	// Unset modfunc, values & redirect URL.
	RedirectURL( [ 'modfunc' , 'values' ] );
}

if ( $_REQUEST['modfunc'] === 'update_elements' )
{
	if ( ! empty( $_REQUEST['values'] )
		&& ! empty( $_POST['values'] )
		&& ! empty( $_REQUEST['id'] )
		&& AllowEdit() )
	{
		foreach ( (array) $_REQUEST['values'] as $id => $columns )
		{
			if ( ( ! empty( $columns['WIDTH'] )
					&& (string) (int) $columns['WIDTH'] !== $columns['WIDTH'] )
				|| ( ! empty( $columns['HEIGHT'] )
					&& (string) (int) $columns['HEIGHT'] !== $columns['HEIGHT'] ) )
			{
				// Fix SQL error invalid input syntax for type integer
				$error[] = _( 'Please enter valid Numeric data.' );

				continue;
			}

			if ( ! empty( $columns['OPTIONS'] ) )
			{
				if ( ! empty( $columns['OPTIONS']['CSS'] ) )
				{
					// Sanitize CSS.
					$columns['OPTIONS']['CSS'] = DashboardsSanitizeCSS(
						DBUnescapeString( $columns['OPTIONS']['CSS'] )
					);
				}

				// Options: JSON.
				// @since RosarioSIS 14.0 Prepared SQL statements: no need to escape string
				$columns['OPTIONS'] = json_encode( $columns['OPTIONS'] );

				if ( ! function_exists( 'db_query_prep' ) )
				{
					// Maintain backward compatibility.
					$columns['OPTIONS'] = DBEscapeString( $columns['OPTIONS'] );
				}
			}

			if ( $id !== 'new' )
			{
				DBUpdate(
					'resources_dashboard_elements',
					$columns,
					[ 'ID' => (int) $id, 'DASHBOARD_ID' => (int) $_REQUEST['id'] ]
				);
			}

			// New: check for URL, Width & Height.
			elseif ( ! empty( $columns['URL'] )
				&& ! empty( $columns['WIDTH'] )
				&& ! empty( $columns['HEIGHT'] ) )
			{
				// Remove RosarioSIS URL from URL.
				$columns['URL'] = str_replace(
					RosarioURL(),
					'',
					$columns['URL']
				);

				$resource_id = DBInsert(
					'resources_dashboard_elements',
					[ 'TYPE' => 'iframe', 'DASHBOARD_ID' => (int) $_REQUEST['id'] ] + $columns,
					'id'
				);
			}
		}
	}

	// Unset modfunc, values & redirect URL.
	RedirectURL( [ 'modfunc' , 'values' ] );
}

if ( $_REQUEST['modfunc'] === 'remove'
	&& AllowEdit() )
{
	if ( DeletePrompt( dgettext( 'Dashboards', 'Dashboard' ) ) )
	{
		DBQuery( "DELETE FROM resources_dashboards
			WHERE ID='" . (int) $_REQUEST['id'] . "'" );

		$modname = 'Dashboards/Dashboard.php&id=' . $_REQUEST['id'];

		DBQuery( "DELETE FROM profile_exceptions
			WHERE MODNAME='" . $modname . "'" );

		// Reload left menu so Resource disappears.
		DashboardsReloadMenu();

		// Unset modfunc & ID & redirect URL.
		RedirectURL( [ 'modfunc', 'id' ] );
	}
}

if ( $_REQUEST['modfunc'] === 'remove_element'
	&& AllowEdit() )
{
	if ( DeletePrompt( dgettext( 'Dashboards', 'Dashboard Element' ) ) )
	{
		DBQuery( "DELETE FROM resources_dashboard_elements
			WHERE ID='" . (int) $_REQUEST['element_id'] . "'
			AND DASHBOARD_ID='" . (int) $_REQUEST['id'] . "'" );

		// Unset modfunc & element ID & redirect URL.
		RedirectURL( [ 'modfunc', 'element_id' ] );
	}
}

if ( ! $_REQUEST['modfunc'] )
{
	echo ErrorMessage( $error );

	if ( ! empty( $_REQUEST['id'] )
		&& DBGetOne( "SELECT 1
			FROM resources_dashboards
			WHERE ID='" . (int) $_REQUEST['id'] . "'" ) )
	{
		$title = DBGetOne( "SELECT TITLE
			FROM resources_dashboards
			WHERE ID='" . (int) $_REQUEST['id'] . "'" );

		$elements_RET = DBGet( "SELECT ID,TYPE,URL,SORT_ORDER,WIDTH,HEIGHT,OPTIONS
			FROM resources_dashboard_elements
			WHERE DASHBOARD_ID='" . (int) $_REQUEST['id'] . "'
			ORDER BY SORT_ORDER IS NULL,SORT_ORDER,ID",
		[
			'URL' => 'DashboardsMakeURL',
			'SORT_ORDER' => 'DashboardsMakeTextInput',
			'WIDTH' => 'DashboardsMakeNumberInput',
			'HEIGHT' => 'DashboardsMakeNumberInput',
			'OPTIONS' => 'DashboardsMakeOptions',
		] );

		$columns = [
			'URL' => dgettext( 'Dashboards', 'URL' ),
			'SORT_ORDER' => _( 'Sort Order' ),
			'WIDTH' => dgettext( 'Dashboards', 'Width (%)' ),
			'HEIGHT' => dgettext( 'Dashboards', 'Height (px)' ),
			'OPTIONS' => _( 'Options' ),
		];

		$link['add']['html'] = [
			'URL' => DashboardsMakeURL( '', 'URL' ),
			'SORT_ORDER' => DashboardsMakeTextInput( '', 'SORT_ORDER' ),
			'WIDTH' => DashboardsMakeNumberInput( '', 'WIDTH' ),
			'HEIGHT' => DashboardsMakeNumberInput( '', 'HEIGHT' ),
			'OPTIONS' => DashboardsMakeOptions( '', 'OPTIONS' ),
		];

		$link['remove']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'] .
			'&id=' . $_REQUEST['id'] . '&modfunc=remove_element';
		$link['remove']['variables'] = [ 'element_id' => 'ID' ];

		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] .
				'&id=' . $_REQUEST['id'] . '&modfunc=update_elements' ) . '" method="POST">';

		// Configuration.
		DrawHeader(
			'<a href="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] ) . '">« ' . _( 'Back' ) . '</a> ' .
			$title,
			SubmitButton()
		);

		$note[] = dgettext(
			'Dashboards',
			'To disable responsive list layout, add <code>&LO_disable_responsive=Y</code> to the URL'
		);

		echo ErrorMessage( $note, 'note' );

		ListOutput(
			$elements_RET,
			$columns,
			dgettext( 'Dashboards', 'Dashboard Element' ),
			dgettext( 'Dashboards', 'Dashboard Elements' ),
			$link
		);

		echo '<div class="center">' . SubmitButton() . '</div>';
		echo '</form>';
	}
	else
	{
		$resources_RET = DBGet( "SELECT ID,TITLE,ID AS CONFIGURATION
			FROM resources_dashboards
			ORDER BY TITLE,ID",
		[
			'TITLE' => 'DashboardsMakeTextInput',
			'CONFIGURATION' => 'DashboardsMakeConfiguration',
		] );

		$columns = [
			'TITLE' => _( 'Title' ),
			'CONFIGURATION' => _( 'Configuration' ),
		];

		$link['add']['html'] = [
			'TITLE' => DashboardsMakeTextInput( '', 'TITLE' ),
			'CONFIGURATION' => '',
		];

		$link['remove']['link'] = 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=remove';
		$link['remove']['variables'] = [ 'id' => 'ID' ];

		echo '<form action="' . URLEscape( 'Modules.php?modname=' . $_REQUEST['modname'] . '&modfunc=update' ) . '" method="POST">';
		DrawHeader( '', SubmitButton() );

		echo ErrorMessage( $note, 'note' );

		ListOutput(
			$resources_RET,
			$columns,
			dgettext( 'Dashboards', 'Dashboard' ),
			dgettext( 'Dashboards', 'Dashboards' ),
			$link,
			[],
			[ 'valign-middle' => true ]
		);

		echo '<div class="center">' . SubmitButton() . '</div>';
		echo '</form>';
	}
}
