<?php
/**
 * Menu.php file
 * Required
 * - Add Menu entries to other modules
 *
 * @package Dashboards module
 */

// Use dgettext() function instead of _() for Module specific strings translation.
// See locale/README file for more information.

// Add a Menu entry to the Resources module.
if ( $RosarioModules['Resources'] ) // Verify Resources module is activated.
{
	$menu['Resources']['admin']['Dashboards/Dashboards.php'] = dgettext( 'Dashboards', 'Dashboards' );

	$menu_resources_RET = DBGet( "SELECT ID,TITLE
		FROM resources_dashboards
		ORDER BY TITLE" );

	// Add Dashboards.
	foreach ( (array) $menu_resources_RET as $resource )
	{
		$resource_modname = 'Dashboards/Dashboard.php&id=' . $resource['ID'];

		$menu['Resources']['admin'][ $resource_modname ] = $resource['TITLE'];
		$menu['Resources']['teacher'][ $resource_modname ] = $resource['TITLE'];
	}
}
