
/**********************************************************************
 install_mysql.sql file
 Required as the module adds programs to other modules
 - Add profile exceptions for the module to appear in the menu
 - Add resources_dashboards table
***********************************************************************/

/*******************************************************
 profile_id:
 	- 0: student
 	- 1: admin
 	- 2: teacher
 	- 3: parent
 modname: should match the Menu.php entries
 can_use: 'Y'
 can_edit: 'Y' or null (generally null for non admins)
*******************************************************/
--
-- Data for Name: profile_exceptions; Type: TABLE DATA;
--

INSERT INTO profile_exceptions (profile_id, modname, can_use, can_edit)
SELECT 1, 'Dashboards/Dashboards.php', 'Y', 'Y'
FROM DUAL
WHERE NOT EXISTS (SELECT profile_id
    FROM profile_exceptions
    WHERE modname='Dashboards/Dashboards.php'
    AND profile_id=1);


--
-- Name: resources_dashboards; Type: TABLE;
--

CREATE TABLE resources_dashboards (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title text NOT NULL,
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp NULL ON UPDATE current_timestamp
);


--
-- Name: resources_dashboard_elements; Type: TABLE; Schema: public; Owner: rosariosis; Tablespace:
--

CREATE TABLE resources_dashboard_elements (
    id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    dashboard_id integer NOT NULL,
    FOREIGN KEY (dashboard_id) REFERENCES resources_dashboards(id),
    type varchar(255) NOT NULL, -- Type: only "iframe" for now
    url text NOT NULL,
    sort_order numeric,
    width integer NOT NULL,
    height integer NOT NULL,
    options text, -- JSON
    created_at timestamp DEFAULT current_timestamp,
    updated_at timestamp
);
